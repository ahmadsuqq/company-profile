/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        poppins: ["Poppins", "sans-serif"],
      },
      colors: {
        primary: "rgb(255, 255, 255)",
        secondary: "#343434",
        accent: "#a8a8a8",
      },
    },
  },
  plugins: [
    // ...
    // eslint-disable-next-line no-undef
    require("tailwind-scrollbar"),
  ],
};
