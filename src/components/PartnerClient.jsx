import Bcp from "../assets/patnerAndClient/bcp.svg";
import Redikari from "../assets/patnerAndClient/redikari.svg";
import Millenia from "../assets/patnerAndClient/millenia.svg";
import Awstech from "../assets/patnerAndClient/awstech.svg";
import Taruna1 from "../assets/patnerAndClient/taruna1.svg";
import Taruna from "../assets/patnerAndClient/taruna.svg";
import Gis from "../assets/patnerAndClient/gis.svg";
import PelitaHati from "../assets/patnerAndClient/pelita-hati.svg";

const patnerAndClient = [
  {
    title: "Bahtera Cipta Perkasa",
    logo: Bcp,
  },
  {
    title: "Radikari",
    logo: Redikari,
  },
  {
    title: "Millenia",
    logo: Millenia,
  },
  {
    title: "Awstech",
    logo: Awstech,
  },
  {
    title: "SDK Taruna Terpadu 1",
    logo: Taruna1,
  },
  {
    title: "SMK Taruna Terpadu",
    logo: Taruna,
  },
  {
    title: "Global Insani School",
    logo: Gis,
  },
  {
    title: "Yayasan Pelita Hati",
    logo: PelitaHati,
  },
];

export default function PartnerClient() {
  return (
    <div className="w-full md:max-h-screen space-y-14 px-6 py-32" id="client">
      <div className="flex flex-col justify-center w-full h-full gap-6">
        <div className="text-3xl font-bold">
          <h5>Client & Partner</h5>
        </div>
        <div className="w-full md:overflow-x-scroll md:overflow-y-hidden scrollbar-thin">
          <div className="grid grid-cols-2 justify-items-center md:flex items-center gap-4 md:w-[1900px] lg:w-[2500px] md:h-[400px]">
            {patnerAndClient.map((item, i) => (
              <div key={i} className="bg-[#d9d9d9] flex flex-col justify-center items-center rounded-xl w-[150px] h-[150px] text-center font-semibold text-black gap-2 sm:w-[200px] sm:h-[230px] lg:w-[272px] md:h-[250px]">
                <img src={item.logo} alt="partner" className="w-full h-[50%] px-4 pt-4" />
                <h5 className="py-3">{item.title}</h5>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
