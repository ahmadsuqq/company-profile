import icon1 from "../assets/icon1.svg";
import icon2 from "../assets/icon2.svg";
import icon3 from "../assets/icon3.svg";

const jobItems = [
  {
    title: "Creativity",
    logo: icon1,
    des: "Our team members are encouraged to bring their whole selves to work, embracing their unique backgrounds, experiences,",
  },
  {
    title: "Availability",
    logo: icon2,
    des: "At our tech company, availability is a top priority, and we work tirelessly to ensure that our products and services are always accessible to our customers.",
  },
  {
    title: "Functionality",
    logo: icon3,
    des: "From ideation to implementation, we prioritize functionality as a key consideration in every aspect of our development process, from architecture and design to testing and optimization.",
  },
];

export default function WhoWeAre() {
  return (
    <div className="w-full lg:h-screen flex flex-col justify-center items-center px-6 py-20 space-y-10" id="whoWeAre">
      <div className="w-full lg:max-w-[70vw] space-y-4 text-center">
        <h5 className="text-3xl font-bold">Who We Are</h5>
        <div className="bg-accent rounded-full p-3 w-[176px] m-auto font-semibold">
          <h5>How We Work</h5>
        </div>
        <p>We are a company that is deeply committed to our customers, and we strive to build long-lasting relationships with them by providing exceptional service and support.</p>
      </div>
      <div className="flex flex-wrap justify-center gap-3 sm:gap-6 w-full md:gap-0 md:grid grid-cols-3 justify-items-center lg:w-[90vw] xl:w-[75vw] bg--400">
        {jobItems.map((res, i) => (
          <div key={i} className={`w-[157px] sm:w-[200px] md:w-[220px] lg:w-[300px] lg:h-[302px] bg-secondary rounded-lg text-sm px-3 py-4 sm:py-6 flex justify-center items-center flex-col space-y-3 shadow-2xl text-center container-job`}>
            <div className="flex justify-center items-center w-full flex-col gap-2 text-lg font-semibold ">
              <img src={res.logo} alt="logo" className="w-8 h-8" />
              <h5>{res.title}</h5>
            </div>
            <p>{res.des}</p>
          </div>
        ))}
      </div>
    </div>
  );
}
