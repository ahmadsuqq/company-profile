import { Dropdown } from "antd";
import { GiHamburgerMenu } from "react-icons/gi";

const navItems = [
  {
    title: "Home",
    href: "#home",
    key: "0",
  },
  {
    title: "About Us",
    href: "#about",
    key: "1",
  },
  {
    title: "Our Client & Partner",
    href: "#client",
    key: "2",
  },
  {
    title: "Contact",
    href: "#contact",
    key: "3",
  },
];

export default function MyNavbar() {
  const items = [
    {
      label: <a href="#home">Home</a>,
      key: "0",
    },
    {
      label: <a href="#about">About Us</a>,
      key: "1",
    },
    {
      label: <a href="#client">Our Client & Partner</a>,
      key: "2",
    },
    {
      label: <a href="#contact">Contact</a>,
      key: "3",
    },
  ];

  return (
    <nav className="bg-secondary w-screen px-10 lg:px-20 py-4 fixed top-0 left-0 right-0 z-50 flex justify-between items-center shadow-lg">
      <div className="w-[34px] lg:w-[57px] h-full ">
        <img src="/ezziWork.png" alt="logo" />
      </div>
      <ul className=" gap-8 hidden sm:flex items-center text-sm lg:text-base">
        {navItems.map((item, i) => {
          return (
            <li key={i}>
              <a href={item.href}>{item.title}</a>
            </li>
          );
        })}
      </ul>
      {/* hamburger menu */}
      <div className="sm:hidden flex justify-between items-center">
        <Dropdown
          menu={{
            items,
          }}
          trigger={["click"]}
        >
          <a onClick={(e) => e.preventDefault()} className="text-[24px]">
            <GiHamburgerMenu />
          </a>
        </Dropdown>
      </div>
    </nav>
  );
}
