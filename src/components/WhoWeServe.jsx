const serveItem = [
  {
    title: "CORPORATE ",
    des: "Helping corporations to identify and implement innovative  solutions to drive sustainable growth.",
  },
  {
    title: "ORGANIZATION",
    des: "Empowering organizations with innovative technology and  resources to improve operations and enhance efficiency.",
  },
  {
    title: "SCHOOL & EDUCATION ",
    des: "Empowering schools with advanced technology and  resources to enhance teaching methods and engage  students.",
  },
  {
    title: "NON-PROFIT",
    des: "Empowering non-profit organizations with innovative  technology and resources to enhance operations and  streamline processes",
  },
];

export default function WhoWeServe() {
  return (
    <div className="w-full px-6 flex flex-col pt-20 gap-14 items-center overflow-x-hidden" id="whoWeServe">
      <div className="w-full space-y-4 text-center">
        <h5 className="text-3xl font-bold">Who We Serve</h5>
        <div className="bg-accent rounded-full p-3 w-[176px] m-auto font-semibold">
          <h5>How We Work</h5>
        </div>
        <p>We are a company that is deeply committed to our customers, and we strive to build long-lasting relationships with them by providing exceptional service and support.</p>
      </div>
      <div className="w-full md:overflow-x-scroll scrollbar-thin">
        <div className="grid grid-cols-2 gap-3 md:flex justify-center items-center md:gap-6 md:w-[1500px] my-10">
          {serveItem.map((res, i) => (
            <div key={i} className="w-full h-full md:h-[252px] md:w-[400px] text-center rounded-3xl bg-secondary px-3 py-6 space-y-6">
              <h5 className="font-semibold text-lg">{res.title}</h5>
              <p>{res.des}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
