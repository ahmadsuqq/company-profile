import logo1 from "../assets/features/logo6.svg";
import logo2 from "../assets/features/logo3.svg";
import logo3 from "../assets/features/logo2.svg";
import logo4 from "../assets/features/logo5.svg";
import logo5 from "../assets/features/logo1.svg";
import logo6 from "../assets/features/logo4.svg";
import mansion from "../assets/mansion.png";

const productFeaturesItem = [
  {
    title: "Self Assessment",
    logo: logo1,
    des: "Improve student’s potential with our aptitude test, a comprehensive tool that helps the students discover their natural abilities and interests.",
  },
  {
    title: "Daily Attendance",
    logo: logo2,
    des: "Simplify attendance tracking with QR Scan presences & tracking by GPS.",
  },
  {
    title: "Edutainment Program",
    logo: logo3,
    des: "Learning has never been more fun! Experience the perfect blend of education and entertainment with our edutainment programs.",
  },
  {
    title: "AI Integration",
    logo: logo4,
    des: "Transform the way of teaching that leverages advanced AI technology to provide real-time insights, feedback, and assessment.",
  },
  {
    title: "Dashboard Information System",
    logo: logo5,
    des: "Real-time school’s performance monitor by providing comprehensive insights into student ata, financials, and administrative tasks.",
  },
  {
    title: "Learning Management System",
    logo: logo6,
    des: "A flexible and adaptive learning environment that meets the needs of modern learners.",
  },
];

export default function ProductFeatures() {
  return (
    <div className="w-full px-6 pt-32 space-y-20 " id="product">
      <div className=" w-full text-center space-y-6 ">
        <h5 className="text-3xl font-bold">Product Feature</h5>
        <p>School Information System and Apps</p>
      </div>
      <div className="grid grid-cols-2 gap-3 justify-items-center lg:gap-10 lg:w-[88vw] mx-auto relative">
        {productFeaturesItem.map((res, i) => (
          <div key={i} className="w-full md:w-[323px] md:h-[250px] lg:w-[460px] lg:h-[350px] flex flex-col items-center text-center rounded-xl bg-secondary px-3 py-6 space-y-4 z-20">
            <div className="flex flex-col sm:justify-center items-center gap-4 w-full h-full">
              <div className="flex flex-col justify-center items-center gap-4">
                <img src={res.logo} alt="logo" className="w-10 h-10" />
                <h5 className="font-semibold text-lg">{res.title}</h5>
              </div>
              <p className="lg:max-w-[25vw]">{res.des}</p>
            </div>
          </div>
        ))}
        <div className=" absolute bottom-0 -left-20 z-10">
          <img src={mansion} alt="mansion" />
        </div>
      </div>
    </div>
  );
}
