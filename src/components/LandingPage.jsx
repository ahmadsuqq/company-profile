import { Button } from "antd";
import mansion from "../assets/mansion.png";

// eslint-disable-next-line react/prop-types
export default function LandingPage({ handleClickGetStarted }) {
  return (
    <div className="w-full h-screen bg-black flex flex-col lg:flex-row justify-evenly pt-20 items-center gap-6 " id="home">
      <div className="px-6 lg:pl-20 w-full space-y-3">
        <div className="w-full space-y-4">
          <img src="/ezziWork.png" alt="" className="w-[67px] lg:w-[132px] h-[49px] lg:h-[96px]" />
          <h1 className="text-3xl font-semibold">Company Profile</h1>
        </div>
        <p className="max-w-[60vw]">Welcome to the Ezzi website, where we are dedicated to assisting you with your business needs and challenges through our services.</p>
        <div className="">
          <Button className="bg-accent rounded-full text-white font-semibold outline-none border-none hover:!bg-accent hover:!text-white" onClick={() => handleClickGetStarted()}>
            Get Started
          </Button>
        </div>
      </div>
      <div className="w-full flex justify-end lg:w-[610px] lg:h-[596px]">
        <img src={mansion} alt="logo" />
      </div>
    </div>
  );
}
