import { FaEnvelope, FaGlobe, FaPhone } from "react-icons/fa";
import { FaLocationDot } from "react-icons/fa6";

const contact = [
  {
    label: "info@ezzi.co.id",
    logo: <FaEnvelope />,
    href: "",
  },
  {
    label: "0821 3113 3221",
    logo: <FaPhone />,
  },
  {
    label: "www.ezzi.co.id",
    logo: <FaGlobe />,
    href: "https://www.ezzi.co.id/",
  },
];

export default function Footer() {
  return (
    <footer className="flex w-full justify-between px-3 lg:px-20 md:px-8 py-6 md:py-10 bg-secondary" id="contact">
      <div className="flex flex-col gap-4">
        <h5 className=" font-bold text-sm sm:text-lg md:text-2xl">PT.ELANG SYSTEM SOLUSI INDONESIA</h5>
        <div className=" text-xs space-y-2 sm:text-sm lg:text-base">
          <FaLocationDot className="!text-red-600 !text-xl" />
          <p>
            JL H JAPAT II NO 77A RT 004 RW 001, ABADIJAYA, <br /> KEC SUKMAJAYA, KOTA DEPOK, JAWA BARAT
          </p>
        </div>
      </div>
      <div className="flex flex-col items-start justify-start gap-3 h-full">
        <h5 className=" font-semibold text-sm sm:text-base lg:text-xl">CONTACT US</h5>
        <ul className="space-y-2">
          {contact.map((res, i) => (
            <li key={i}>
              <a href={res.href} onClick={() => res.handleClick()} target="_blink" className="flex items-center gap-3 text-xs sm:text-sm">
                {res.logo}
                <span>{res.label}</span>
              </a>
            </li>
          ))}
        </ul>
      </div>
    </footer>
  );
}
