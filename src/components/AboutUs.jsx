import MansionRight from "../assets/mansion-right.png";
import MansionLeft from "../assets/mansion-left.png";

export default function AboutUs() {
  const visiMisiItem = [
    {
      title: "Our Vision",
      des: "Our vision is to revolutionize industries and transform the way businesses operate through the use of cutting-edge technology. We transform the industries with technology.",
    },
    {
      title: "Our Mission",
      des: "Our mission is to help all the business or organization needs by providing them with our product & services that simplifies their lives and saves them time.",
    },
  ];
  return (
    <div className="bg-black w-full pt-32 flex flex-col justify-center items-center gap-6" id="about">
      <div className="w-full space-y-5 text-center px-6 lg:px-20">
        <h5 className="text-3xl font-bold">About Us</h5>
        <p>
          EzziSchool is an apps build by a fast-growing tech startup company focused on the automation & management technologies with innovative products & services. Our mission is to help all the business or organization needs by providing
          them with our product & services that simplifies their lives and saves them time. With a team of experienced professionals, we are committed to delivering high-quality product & service that exceeds our customers&#39;
          expectations. We pride ourselves on being a socially responsible company, committed to making a positive impact on our community and the environment.
        </p>
      </div>
      <div className="flex gap-2 sm:gap-4 lg:gap-6 px-6 mt-14">
        {visiMisiItem.map((item, i) => (
          <div key={i} className="w-full md:h-[234px] lg:w-[440px] bg-secondary rounded-md shadow-md shadow-black py-12 space-y-4 text-sm lg:text-base p-2 text-center">
            <h5 className="text-xl font-bold">{item.title}</h5>
            <p>{item.des}</p>
          </div>
        ))}
      </div>
      <div className="flex justify-between items-center w-full">
        <img src={MansionLeft} alt="" className="w-[123px] h-[226px] lg:h-[325px]" />
        <img src={MansionRight} alt="" className="w-[123px] h-[226px]lg:h-[325px]" />
      </div>
    </div>
  );
}
