import AboutUs from "./components/AboutUs";
import LandingPage from "./components/LandingPage";
import MyNavbar from "./components/MyNavbar";
import PartnerClient from "./components/PartnerClient";
import ProductFeatures from "./components/ProductFeatures";
import WhoWeAre from "./components/WhoWeAre";
import WhoWeService from "./components/WhoWeServe";
import Footer from "./components/Footer";
import { useRef } from "react";

function App() {
  const goToAbout = useRef(null);

  const handleClickGetStarted = () => {
    goToAbout.current.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <div
      className="
      font-poppins text-primary bg-black scrollbar-thumb-secondary scrollbar-track-accent"
    >
      <MyNavbar />
      <LandingPage handleClickGetStarted={handleClickGetStarted} />
      <div ref={goToAbout}>
        <AboutUs />
      </div>
      <WhoWeAre />
      <WhoWeService />
      <ProductFeatures />
      <PartnerClient />
      <Footer />
    </div>
  );
}

export default App;
